package ru.tinkoff.fintech.product;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.fintech.product.dto.ProductDto;

import javax.validation.Valid;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    // GET localhost:8080/product/1
    @GetMapping("/{id}")
    public ProductDto getById(@PathVariable("id") Long id) {
        return productService.getById(id);
    }

//    GET localhost:8080/product
//    localhost:8080/product?size=10&page=2
    @GetMapping
    public Page<Product> findByPageable(@PageableDefault(size = 5) Pageable pageable) {
        return productService.getByPageable(pageable);
    }

//    PUT localhost:8080/product
    @PutMapping
    public void update(@RequestBody @Valid Product product) {
        productService.update(product);
    }

//    DELETE localhost:8080/product
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        productService.deleteById(id);
    }
}
