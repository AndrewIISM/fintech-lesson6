package ru.tinkoff.fintech.product.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import ru.tinkoff.fintech.product.Product;
import ru.tinkoff.fintech.product.dto.ProductDto;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "title", source = "title"),
            @Mapping(target = "price", source = "price")
    })
    ProductDto fromEntity(Product product);

}
