create table product (
    id bigint primary key auto_increment,
    title varchar2(255) not null,
    price decimal(100, 2)
)